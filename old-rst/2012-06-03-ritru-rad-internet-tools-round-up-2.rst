RITRU: Rad internet tools round up
##################################
:date: 2012-06-03 13:26
:author: mattie
:category: Uncategorized
:slug: ritru-rad-internet-tools-round-up-2

Here’s this weeks round up!

-  `30 of the best vector and illustrations,
   free <http://web.archive.org/web/20130410014542/http://www.webdesigncore.com/2012/05/03/30-of-the-best-vector-and-illustrator-free-resources/>`_
   – More free vector art for the masses!
-  `Pachelbel
   Rant <http://web.archive.org/web/20130410014542/http://www.youtube.com/watch?v=JdxkVQy7QLM>`_
   – I knew Basketcase by Greenday sounded familiar.
-  `On{x} <http://web.archive.org/web/20130410014542/https://www.onx.ms/>`_
   – JavaScript script your Android device.
-  `SCVNGR <http://web.archive.org/web/20130410014542/http://techcrunch.com/2010/08/25/scvngr-game-mechanics/0/08/25/scvngr-game-mechanics/>`_-
   Secret Game Mechanics Playbook. Interesting look into how Zynga
   develops their games and other developers.

