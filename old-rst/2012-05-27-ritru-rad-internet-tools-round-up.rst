RITRU: Rad internet tools round up
##################################
:date: 2012-05-27 13:24
:author: mattie
:category: Uncategorized
:slug: ritru-rad-internet-tools-round-up

MORE COOL STUFF ON THE INTERNET

-  `15 Rad Robot
   Accessories <http://mashable.com/2012/05/15/robot-accessories-office/#6390510-Robot-Calculator>`_
   – Kick ass robot desktoys and the like. (Via @Kasey\_L/@Mashable)
-  `This Is Why I Am
   Broke <http://www.thisiswhyimbroke.com/>`_
   – Pretty much.
-  `ifttt <http://ifttt.com/>`_
   – Put the internet to work for you.
-  `Bulk Rename
   Utility <http://www.bulkrenameutility.co.uk/>`_
   – A pretty good bulk renaming utility, if you excuse the terrible
   clutter.
-  `Cookoo
   Watch <http://www.engadget.com/2012/05/24/insert-coin-cookoo>`_
   – Watch for geeks who want to keep it subtle.
-  `Whiskey
   Media <http://api.giantbomb.com/>`_
   – An API for Comics and Games.
-  `Procatinator <http://procatinator.com/>`_
   – CATS AND MUSIC!


-  `20 Examples of Stereographics that Grab the
   Attention <http://www.dzinepress.com/2012/05/20-examples-of-stereographics-that-grab-the-attention/>`_
-  `Iconspedia <http://www.iconspedia.com/>`_-
   A collection of free icons.


-  `navigator.connection <http://davidbcalhoun.com/2010/using-navigator-connection-android>`_
   – Optimizing based on connection speed: using navigator.connection on
   Android 2.2+ (Via @m4rkmc)
-  `Automated Testing of ASP.NET MVC
   Applications <http://t.co/In1oarNY>`_
   – Via @uluhonolulu

