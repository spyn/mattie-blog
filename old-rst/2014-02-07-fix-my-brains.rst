Fix my brains
#############
:date: 2014-02-07 23:00
:author: mattie
:category: Health, Mental
:tags: brains, health, mental health, socphob
:slug: fix-my-brains

On Thursday I decided to reach out to my GP and talk to him about my
mental health. For the last year and a bit I've felt a lot more
secluded, quiet and less social. Although, I've always been quiet in
social situations, I have felt this more so in the last few months.

Over the last couple of weeks I've chatted to some of my gaming buddies
about the situation I feel like I'm in. One of the guys, whom I've known
for a very long time posted about his condition and how EMDR therapy
helped him quite significantly, along with talking to a psychologist and
that he is doing a lot better than he has been before. His story gave me
a more faith in 'seeing someone about my problems'. Also, over the last
couple of weeks I've had my bestie come over from Melbourne and we
talked about mental health, as she's studying to be a nurse, I made a
promise to make sure I ended up seeing someone to get the ball rolling.

I've always felt a bit 'out of it' when I'm around people. Recently I've
just frozen up during conversations, not sure how to continue, my mind
literally goes blank. This has started happening a lot more, even when
talking to people I know very well and enjoy their company. I guess
that's what triggered me seeking more information. I haven't always been
so quiet, it's almost like I'm not myself. I'm not even making hilarious
puns as often anymore, I'm like a piece of flint that's lost it's..
spark.

Anyway, back to the GP, talked to him, a quick checklist of things could
indicate a social phobia, which most my symptoms contain. He gave me 3
suggested ways of going about the treatment. Group therapy, CBT or
medication. Also he gave me a test to do once I got home.

So, right now I'm looking into treatments and seeing what would benefit
me the most. Leaning more towards CBT. I'd rather fix my way of thinking
then take a gunshot approach.

I'll continue to post my progress here.
