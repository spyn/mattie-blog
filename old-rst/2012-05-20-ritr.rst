RITRU: Rad internet tools round up
##################################
:date: 2012-05-20 13:23
:author: mattie
:category: Uncategorized
:slug: ritr

Here’s a few things I’ve found useful on the internet this week;

-  `Audio
   Tool <http://web.archive.org/web/20120601062815/http://audiotool.com/>`_
   – Create music with your web browser.
-  `Javascript
   Piano <http://web.archive.org/web/20120601062815/http://t.co/8eBIDUJ0>`_
   – A piano written in JavaScript. (via @ZenPsycho)
-  `Leaflet <http://web.archive.org/web/20120601062815/http://leaflet.cloudmade.com/>`_
   - Create JavaScript maps similar to Google Maps – without having to
   worry about purchasing a license.
-  `Data Visualisation
   Tools <http://web.archive.org/web/20120601062815/http://selection.datavisualization.ch/>`_
   – A collection of web based tools you can put together some awesome
   data visualisations.
-  `Stamen <http://web.archive.org/web/20120601062815/http://maps.stamen.com/>`_
   – Water colour style maps using
   `OpenStreetMap <http://web.archive.org/web/20120601062815/http://openstreetmap.org/>`_.
-  `OpenSignalMaps <http://web.archive.org/web/20120601062815/http://opensignalmaps.com/>`_
   – Find the best mobile network in your area.

.. raw:: html

   <div>

.. raw:: html

   </div>

.. raw:: html

   <div>

-  `Alltop
   Infographics <http://web.archive.org/web/20120601062815/http://infographics.alltop.com/>`_
   – A collection of links related to infographics.
-  `Easel.ly <http://web.archive.org/web/20120601062815/http://www.easel.ly/>`_
   - Create infographics online easily.
-  `Venngage <http://web.archive.org/web/20120601062815/http://venngage.com/>`_
   - Create custom infographics online, engage your views and track your
   results.
-  `Infogram <http://web.archive.org/web/20120601062815/http://infogr.am/>`_
   - Create interactive infographics.

.. raw:: html

   <div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div>

-  `SchemaVerse <http://web.archive.org/web/20120601062815/http://schemaverse.com/>`_
   - The Schemaverse is a space-based strategy game implemented entirely
   within a PostgreSQL database.
-  `HADOOP <http://web.archive.org/web/20120601062815/http://hadoop.apache.org/>`_
   – Open source, scalable, distributed computing.
-  `Mechanical
   Turk <http://web.archive.org/web/20120601062815/https://www.mturk.com/mturk/welcome>`_
   - Amazon’s Mechanical Turk, crowd sourcing.
-  `Microworkers <http://web.archive.org/web/20120601062815/http://microworkers.com/>`_
   – Crowd sourcing little things, such as tweets, facebook likes and
   other marketing related things.

.. raw:: html

   <div>

.. raw:: html

   </div>

.. raw:: html

   </div>

-  `15 Useful Web Apps for
   Designers <http://web.archive.org/web/20120601062815/http://webdesignledger.com/tools/15-useful-web-apps-for-designers>`_
   - A bunch of cool web apps for designers or designer at mind.
-  `40 High Quality Free XHTML/CSS Web Template for
   Developers <http://web.archive.org/web/20120601062815/http://djdesignerlab.com/2011/07/06/40-high-quality-free-xhtmlcss-web-template-for-developers/>`_
   - Some free high quality templates.
-  `101 Apps for Your Web App Startup
   Toolbox <http://web.archive.org/web/20120601062815/http://web.appstorm.net/roundups/freelancing-tools/101-apps-for-your-web-app-startup-toolbox/>`_
   – Just as the title suggests, there’s quite a few of them.
-  `Free
   Vectors <http://web.archive.org/web/20120601062815/http://www.vectorstock.com/free-vectors>`_
   - A whole bunch of free to use vectors presented in thumbnails.
-  `Handwriting
   Fonts <http://web.archive.org/web/20120601062815/http://desktoppub.about.com/od/freefonts/tp/Free_Handwriting_School_Fonts.htm>`_
   – Hand writing fonts for kids to trace over.

