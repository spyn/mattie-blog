Automate my everythings
#######################
:date: 2012-06-13 13:28
:author: mattie
:category: Uncategorized
:slug: automate-my-everythings

I spent some of the day, between tasks, to take a look at “if this then
that” or otherwise known as
`ifttt <http://web.archive.org/web/20130410014542/http://ifttt.com/>`_.

`ifttt <http://web.archive.org/web/20130410014542/http://ifttt.com/>`_
allows you to essentially script events to actions. For example, I wrote
a `recipe that emails
me <http://web.archive.org/web/20130410014542/http://ifttt.com/recipes/40275>`_
when`Gizmodo’s App
Deals <http://web.archive.org/web/20130410014542/http://www.gizmodo.com.au/tags/app-deals-of-the-day/>`_
gets posted, then tells me what mobile applications are on sale for the
day via email.

I got a bit excited and went through some of the recipes that were on
display and ended up with some tasks that archived all my tweets into
evernote and my favorites. I’m sure I’ll go over it tonight to see what
else I can do with it that would be relevant to my interests.

There’s also
`zapier.com <http://web.archive.org/web/20130410014542/http://zapier.com/>`_
(Thanks Jordan S) which can also do the same thing, but with different
“channels/apps” most noted, GitHub and JIRA!

For mobile users, there’s
`on{x} <http://web.archive.org/web/20130410014542/https://www.onx.ms/>`_,
where you can script events on your phone to do certain things. Best bit
is that it’s written in JavaScript – so you can do quite a bit with it.
I haven’t played around with it yet, but I’m sure I will in the near
future.
