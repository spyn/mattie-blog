I got my wheels back
####################
:date: 2010-05-19 23:57
:author: mattie
:category: Skating
:tags: rollerdrome, skates, Skating
:slug: i-got-my-wheels-back

[flickr pid="4621403349" size="medium"]

Tonight I decided to hit the Rollerdrome for my now usual fortnightly
skate. I sat down tied my skates up then hit the rink. The first thing I
do when I get on the rink is to check the grip and holy shit -- my
wheels did not stick to the ground. This is quite awesome because it
meant one thing. The last times I've been skating I haven't been able to
powerslide properly; powersliding is where you put all your weight on
your wheels going the opposite side to where you were skating, it also
makes loud noises and sometimes sparks (if you're lucky).

This was so great. It meant that I had pretty much the best control I
could have. Reminded me of back when I used to skate at Rolloways as a
teenager being able to do freaky skate shit, like diving in between
clumps of people who are slowly skating around. Catching that half a
second window you get between people and sneaking up to someone and
letting a massive sounding powerslide go.

I've been skating for a pretty long time, ever since I was 13. I took it
up because I really slow at running and I needed something to do in my
spare time; skating seemed pretty good. Started with the learn to skate
program, did all the star classes and ended up pretty decent at skating.
(I will not mention that it was a figure skating class either... then
again I was one of the few people that figure skated in speed skates)

It's just so exciting feeling the air rush through you, knowing you are
in complete control and you can recover from any slight slip-up with mad
balance control.

[flickr pid="4611494482" size="medium"]

I've been going road skating a fair bit. My recent and longest skate
going at a constant speed (mostly because there was NO ONE at all on the
foot/bike paths) was the following
`RunKeeper <http://www.runkeeper.com>`_ entry:
`http://rnkpr.com/a5rhfd <http://rnkpr.com/a5rhfd%20>`_. Of course, this
skate might of effected my wheels as I was using rinky wheels instead of
road wheels. My axels are fucked okay.

Looking forward to some more skating. Considering being a
`referee <http://wftda.com/officiating>`_ for the `upcoming roller
derby <http://www.perthrollerderby.com.au/>`_ games. I think learning a
massive rulebook to see girls beat and trip each other up would be well
worth it. ;)

And that, is how I roll.
