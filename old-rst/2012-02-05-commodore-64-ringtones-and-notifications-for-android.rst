Commodore 64 ringtones and notifications for Android
####################################################
:date: 2012-02-05 07:26
:author: mattie
:category: Uncategorized
:slug: commodore-64-ringtones-and-notifications-for-android

Last night I compiled a list of tones I would like to use on my phone.
Most of them are based from classic Commodore 64 games. I spent about 4
hours converting SID to WAV, then to MP3 to save space on my phone. Just
thought I would share.

The download link is ad sponsored by
`adf.ly <http://adf.ly/?id=492079>`_, I did this so I could recieve some
credit for my 4 hours work. So deal with it. It’s also about 60MB. So,
enjoy!

`60MB of Commodore 64 ringtones and notification
tones <http://adf.ly/843l1>`_
