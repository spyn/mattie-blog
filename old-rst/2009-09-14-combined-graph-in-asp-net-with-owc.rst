Combined graph in ASP.NET with OWC
##################################
:date: 2009-09-14 11:49
:author: mattie
:category: Code, Tech
:tags: .NET, asp.net, Code, graph, OWC, web development
:slug: combined-graph-in-asp-net-with-owc

It took me a while to find out how to get a hang of making graphs in
ASP.NET combined with OWC. There aren't many places on the internet that
show, or have a good guide. I was having troubles with creating a
combined OWC graph in ASP.NET and I finally worked it out. I thought I
would share. I've left some of the different methods and properties in
there, mostly the styling ones to give a better idea on how to style
your graph to suit you. The DataTable bit is in there also. If you want
to test it straight out you can replace that with an array of your
choosing. 

See https://gist.github.com/spyn/83aebdb9b8ffb8e6a682

Some related links: MSDN Office Web Components Constants <http://msdn.microsoft.com/en-us/library/aa204179%28office.11%29.aspx>

There are probarly some other, better, controls out there.