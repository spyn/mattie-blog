#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = u'mattie'
SITENAME = u'_mattie_id_au'
SITEURL = 'http://mattie.id.au'

PATH = 'content'
THEME = 'themes/nest'
#THEME = 'themes/elegantmod'
TIMEZONE = 'Australia/Perth'
DEFAULT_LANG = u'en'

PLUGIN_PATHS = ['plugins']
PLUGINS = ['sitemap', 'extract_toc', 'tipue_search', 'share_post', 'assets']
MARKUP = ('rst', 'md')

#ARTICLE_URL = '{date:%Y}/{date:%m}/{slug}.html'
#ARTICLE_SAVE_AS = 'content/{date:%Y}/{date:%m}/{slug}.html'
ARTICLE_URL = 'posts/{date:%Y}-{date:%m}-{slug}'
ARTICLE_SAVE_AS = 'posts/{date:%Y}-{date:%m}-{slug}.html'
#ARTICLE_EXCLUDES = ('pages',)
#PAGE_URL = 'pages/{slug}.html'

STATIC_PATHS = [ 
    'extras/robots.txt', 
    'extras/favicon.gif', 
    'extras/.htaccess', 
    'extras/mattie.js',
    'extras/jquery.lifestream.min.js',
    'images'
]

EXTRA_PATH_METADATA = {
    'extras/robots.txt': {'path': 'robots.txt'},
    'extras/favicon.gif': {'path': 'favicon.gif'},
    'extras/.htaccess': {'path': '.htaccess'},
    'extras/mattie.js': {'path': 'js/mattie.js'},
    'extras/jquery.lifestream.min.js': {'path': 'js/jquery.lifestream.min.js'},
    'extras/trianglify-background.svg': {'path': 'trianglify-background.svg'},
}

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None

# Blogroll
LINKS = (
    ('this is mark', 'http://markm.cd/'),
    ('NanoJuice', 'http://nanojuice.net'),
    ('Autodeist', 'http://autodeist.com'),
    ('Sandy Live', 'http://www.sanlive.com/'),
    ('Barge', 'http://barge.moochers.org'),
    ('Pretty Grouse!', 'http://prettygrouse.com/'),
    ('Nick Howson', 'http://nick-howson.github.io'),
)

# Social widget
SOCIAL = (
    ('twitter', 'http://twitter.com/spyn'),
    ('lastfm', 'http://lastfm.com/user/spyn'),
    ('github', 'http://github.com/ametaireau'),
    ('facebook', 'http://facebook.com/spynhugs'),
    ('googleplus', 'https://plus.google.com/+MattspynFinch/'),
    ('youtube', 'http://www.youtube.com/user/evilspyn'),
    ('reddit', 'https://reddit.com/u/spyn'),
)

# Referalls
REFERAL = (
    ('paypal', 'https://paypal.me/spyn'),
    ('acorns', 'http://bit.ly/spynacorn'),
    ('treehouse', 'http://bit.ly/spynTH'),
    ('digitalocean', 'http://bit.ly/spynOcean'),
)

DEFAULT_PAGINATION = 10

GITHUB_URL = 'http://github.com/spyn/'
DISQUS_SITENAME = "mattieidau"
GOOGLE_ANALYTICS = "UA-19229033-2"
TWITTER_USERNAME = "spyn"
FACEBOOK_APP_ID = 336708196445918
FACEBOOK_USERNAME = "spynhugs"


# Uncomment following line if you want document-relative URLs when developing
RELATIVE_URLS = True

# Template
DIRECT_TEMPLATES = (('index', 'tags', 'categories','archives', 'search', 'links', '404', 'lifestream', 'timeline'))
#DIRECT_TEMPLATES = (('index', 'archives', '404'))

# MD_EXTENSIONS = ['codehilite(css_class=highlight)', 'extra', 'headerid', 'toc']
MARKDOWN = {
    'extension_configs': {
        'markdown.extensions.codehilite': { 'css_class': 'highlight'}, 
        'markdown.extensions.extra': {}, 
        'markdown.extensions.meta': {},
    },
    'output_format': 'html5',
}

PAGE_PATHS= ['pages']

RECENT_ARTICLES_COUNT = 10  
#COMMENTS_INTRO = 'Hey, stay a while. Write a comment:'
#SITE_LICENSE = 'string'
SITE_DESCRIPTION  = 'makes u think'
#EMAIL_SUBSCRIPTION_LABEL ('string')
#EMAIL_FIELD_PLACEHOLDER ('string')
#SUBSCRIBE_BUTTON_TITLE ('string')
#MAILCHIMP_FORM_ACTION ('string')
SITESUBTITLE = 'step back, observe, re-engage'
LANDING_PAGE_ABOUT = {'title' : 'Step back, observe, re-engage', 
                      'details' : 'Hi! Welcome to my blog. I\'m working at iiNet as a Developer for CS Tools. ' +
                      'We maintain and develop Toolbox and Signup. Comfortable on quads. Data geek and Lasertag. ' +
                      'Part time proud Dad. Love my Music, Coffee and Rollerskating. Opinions are my own. I enjoy ' +
                      'hacking around, tweeting randomly and playing with tech and games. <span id="lastfm_data"></span><span id="trakt_data"></span>'
                      }
PROJECTS = [
    {
        'name': 'Bamboo and BlinkyTape',
        'url': 'https://github.com/spyn/blinkyTapeProjects',
        'description': 'Scripts to use with BlinkyTape, including Bamboo support'
    },
]

# Plugins

SITEMAP = {
    'format': 'xml',
    'priorities': {
        'articles': 0.5,
        'indexes': 0.5,
        'pages': 0.5
    },
    'changefreqs': {
        'articles': 'monthly',
        'indexes': 'daily',
        'pages': 'monthly'
    }
}

# NEST THEME SPECIFIC
MENUITEMS = [('Categories','/categories'),('Tags','/tags'),('Career', '/timeline'),('Activity','/lifestream')]
NEST_CSS_MINIFY = False
# Add header background image from content/images : 'background.jpg'
NEST_HEADER_IMAGES = 'main_background.jpg'
NEST_HEADER_LOGO = u'/images/logo.gif'
# Footer
NEST_SITEMAP_COLUMN_TITLE = u'Sitemap'
NEST_SITEMAP_MENU = [('Archives', '/archives.html'),('Tags','/tags.html'), ('Authors','/authors.html')]
NEST_SITEMAP_ATOM_LINK = u'Atom Feed'
NEST_SITEMAP_RSS_LINK = u'RSS Feed'
NEST_SOCIAL_COLUMN_TITLE = u'Social'
NEST_LINKS_COLUMN_TITLE = u'Links'
NEST_COPYRIGHT = u'&copy; Matt Finch 2015'
# index.html
NEST_INDEX_HEAD_TITLE = u'Homepage'
NEST_INDEX_HEADER_TITLE = u'360 Degrees'
NEST_INDEX_HEADER_SUBTITLE = u'step back, observe, re-engage'
NEST_INDEX_CONTENT_TITLE = u'Last Posts'
# archives.html
NEST_ARCHIVES_HEAD_TITLE = u'Archives'
NEST_ARCHIVES_HEAD_DESCRIPTION = u'Posts Archives'
NEST_ARCHIVES_HEADER_TITLE = u'Archives'
NEST_ARCHIVES_HEADER_SUBTITLE = u'Archives for all posts'
NEST_ARCHIVES_CONTENT_TITLE = u'Archives'
# article.html
NEST_ARTICLE_HEADER_BY = u'By'
NEST_ARTICLE_HEADER_MODIFIED = u'modified'
NEST_ARTICLE_HEADER_IN = u'in category'
# author.html
NEST_AUTHOR_HEAD_TITLE = u'Posts by'
NEST_AUTHOR_HEAD_DESCRIPTION = u'Posts by'
NEST_AUTHOR_HEADER_SUBTITLE = u'Posts archives'
NEST_AUTHOR_CONTENT_TITLE = u'Posts'
# authors.html
NEST_AUTHORS_HEAD_TITLE = u'Author list'
NEST_AUTHORS_HEAD_DESCRIPTION = u'Author list'
NEST_AUTHORS_HEADER_TITLE = u'Author list'
NEST_AUTHORS_HEADER_SUBTITLE = u'Archives listed by author'
# categories.html
NEST_CATEGORIES_HEAD_TITLE = u'Categories'
NEST_CATEGORIES_HEAD_DESCRIPTION = u'Archives listed by category'
NEST_CATEGORIES_HEADER_TITLE = u'Categories'
NEST_CATEGORIES_HEADER_SUBTITLE = u'Archives listed by category'
# category.html
NEST_CATEGORY_HEAD_TITLE = u'Category Archive'
NEST_CATEGORY_HEAD_DESCRIPTION = u'Category Archive'
NEST_CATEGORY_HEADER_TITLE = u'Category'
NEST_CATEGORY_HEADER_SUBTITLE = u'Category Archive'
# pagination.html
NEST_PAGINATION_PREVIOUS = u'Previous'
NEST_PAGINATION_NEXT = u'Next'
# period_archives.html
NEST_PERIOD_ARCHIVES_HEAD_TITLE = u'Archives for'
NEST_PERIOD_ARCHIVES_HEAD_DESCRIPTION = u'Archives for'
NEST_PERIOD_ARCHIVES_HEADER_TITLE = u'Archives'
NEST_PERIOD_ARCHIVES_HEADER_SUBTITLE = u'Archives for'
NEST_PERIOD_ARCHIVES_CONTENT_TITLE = u'Archives for'
# tag.html
NEST_TAG_HEAD_TITLE = u'Tag archives'
NEST_TAG_HEAD_DESCRIPTION = u'Tag archives'
NEST_TAG_HEADER_TITLE = u'Tag'
NEST_TAG_HEADER_SUBTITLE = u'Tag archives'
# tags.html
NEST_TAGS_HEAD_TITLE = u'Tags'
NEST_TAGS_HEAD_DESCRIPTION = u'Tags List'
NEST_TAGS_HEADER_TITLE = u'Tags'
NEST_TAGS_HEADER_SUBTITLE = u'Tags List'
NEST_TAGS_CONTENT_TITLE = u'Tags List'
NEST_TAGS_CONTENT_LIST = u'tagged'

