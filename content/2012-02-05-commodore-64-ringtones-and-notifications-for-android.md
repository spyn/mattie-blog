Title: Commodore 64 ringtones and notifications for Android
Date: 2012-02-05 07:26
Author: mattie
Category: Music
Tags: C64, Ringtones, Music, Chiptunes
Slug: commodore-64-ringtones-and-notifications-for-android
Summary: Last night I compiled a list of tones I would like to use on my phone

Last night I compiled a list of tones I would like to use on my phone.
Most of them are based from classic Commodore 64 games. I spent about 4
hours converting SID to WAV, then to MP3 to save space on my phone. Just
thought I would share.

The download link is ad sponsored by [adf.ly][], I did this so I could
recieve some credit for my 4 hours work. So deal with it. It’s also
about 60MB. So, enjoy!

[60MB of Commodore 64 ringtones and notification tones][]

  [adf.ly]: http://adf.ly/?id=492079
  [60MB of Commodore 64 ringtones and notification tones]: http://adf.ly/843l1
