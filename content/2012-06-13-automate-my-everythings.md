Title: Automate my everythings
Date: 2012-06-13 13:28
Author: mattie
Category: Systems
Slug: automate-my-everythings
Summary: I spent some of the day looking at IFTTT

I spent some of the day, between tasks, to take a look at “if this then
that” or otherwise known as [ifttt][].

[ifttt][] allows you to essentially script events to actions. For
example, I wrote a [recipe that emails me][] when[Gizmodo’s App Deals][]
gets posted, then tells me what mobile applications are on sale for the
day via email.

I got a bit excited and went through some of the recipes that were on
display and ended up with some tasks that archived all my tweets into
evernote and my favorites. I’m sure I’ll go over it tonight to see what
else I can do with it that would be relevant to my interests.

There’s also [zapier.com][] (Thanks Jordan S) which can also do the same
thing, but with different “channels/apps” most noted, GitHub and JIRA!

For mobile users, there’s [on{x}][], where you can script events on your
phone to do certain things. Best bit is that it’s written in JavaScript
– so you can do quite a bit with it. I haven’t played around with it
yet, but I’m sure I will in the near future.

  [ifttt]: http://ifttt.com/
  [recipe that emails me]: http://ifttt.com/recipes/40275
  [Gizmodo’s App Deals]: http://www.gizmodo.com.au/tags/app-deals-of-the-day/
  [zapier.com]: http://zapier.com/
  [on{x}]: https://www.onx.ms/
