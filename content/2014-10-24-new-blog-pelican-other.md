Title: New blog - using Pelican
Date: 2014-10-24 11:30
Author: mattie
Category: Code
Tags: pelican
Slug: new-blog-pelican-other
Summary: Bye Wordpress, Hello Pelican

So I've decided to finally get rid of Wordpress. Way too much bloat for what I need. I've made the switch to Pelican.

