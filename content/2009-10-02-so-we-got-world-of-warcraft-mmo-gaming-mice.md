Title: So, we got World of Warcraft MMO gaming mice
Date: 2009-10-02 11:23
Author: mattie
Category: Gaming
Slug: so-we-got-world-of-warcraft-mmo-gaming-mice
Summary: Sera decided to purchase a couple of these World of Warcraft MMO gaming mice

Sera decided to purchase a couple of these [World of Warcraft MMO gaming
mice][] for my birthday. They look pretty great, having 15 buttons, cool
glowing OLED that pulsates. It takes a while to get used to the handling
and the design since it’s a bit different to normal, more ergonomic
mice. But apart from that, so far, the hardware acts really well. The
buttons are well placed and it’s very good.

One slight problem. The software that comes with is absolute crap. [It’s
very poorly written.][] It can’t even handle malformed XML and will just
crash. Error handling? Jesus christ learn how to. I tried to simply
import a profile via the armory and I get an AP MFC error. Why? because
the image location has changed or something. How hard would of it been
to, say if there was an error. 1) Display there was an error getting the
image 2) Replace it with a stock image. Oh, the images? they’re STOCK
IMAGES AND DO NOT REALLY CHANGE. You could of distributed them with the
package.

The only problems I have had though, are to do with profile handling.

The other options in the software are okay, such as setting up key
bindings, macros and junk like that. I’ve recently bought my mouse into
work and bound cut, paste, hide desktop, tab forward, tab backward,
build, build project and it works pretty well.

  [World of Warcraft MMO gaming mice]: http://www.steelseries.com/_int/products/partners/
  [It’s very poorly written.]: http://www.fragyou.net/2009/09/24/wow-mmo-gaming-mouse-ap-mfc-error/
