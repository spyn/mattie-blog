Title: RITRU: Rad internet tools round up
Date: 2012-05-27 13:24
Author: mattie
Category: Links
Slug: ritru-rad-internet-tools-round-up
Summary: More cool stuff on the internet

MORE COOL STUFF ON THE INTERNET

-   [15 Rad Robot Accessories][] – Kick ass robot desktoys and the like. (Via @Kasey\_<L/@Mashable>)
-   [This Is Why I Am Broke][] – Pretty much.
-   [ifttt][] – Put the internet to work for you.
-   [Bulk Rename Utility][] – A pretty good bulk renaming utility, if you excuse the terrible clutter.
-   [Cookoo Watch][] – Watch for geeks who want to keep it subtle.
-   [Whiskey Media][] – An API for Comics and Games.
-   [Procatinator][] – CATS AND MUSIC!
-   [20 Examples of Stereographics that Grab the Attention][]
-   [Iconspedia][]-A collection of free icons.
-   [navigator.connection][] – Optimizing based on connection speed: using navigator.connection on Android 2.2+ (Via @m4rkmc)
-   [Automated Testing of ASP.NET MVC Applications][] – Via @uluhonolulu

  [15 Rad Robot Accessories]: http://mashable.com/2012/05/15/robot-accessories-office/#6390510-Robot-Calculator
  [This Is Why I Am Broke]: http://www.thisiswhyimbroke.com/
  [ifttt]: http://ifttt.com/
  [Bulk Rename Utility]: http://www.bulkrenameutility.co.uk/
  [Cookoo Watch]: http://www.engadget.com/2012/05/24/insert-coin-cookoo
  [Whiskey Media]: http://api.giantbomb.com/
  [Procatinator]: http://procatinator.com/
  [20 Examples of Stereographics that Grab the Attention]: http://www.dzinepress.com/2012/05/20-examples-of-stereographics-that-grab-the-attention/
  [Iconspedia]: http://www.iconspedia.com/
  [navigator.connection]: http://davidbcalhoun.com/2010/using-navigator-connection-android
  [Automated Testing of ASP.NET MVC Applications]: http://t.co/In1oarNY

