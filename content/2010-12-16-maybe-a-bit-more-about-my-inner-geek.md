Title: Maybe a bit more about my inner geek
Date: 2010-12-16 13:41
Author: mattie
Category: Personal
Slug: maybe-a-bit-more-about-my-inner-geek
Summary: So I’m guessing a few of you already have seen my entry to iiNet’s TopGeek competition. There’s a bit more it to the brief bio I put on the website.

So I’m guessing a few of you already have seen my entry to [iiNet’s
TopGeek competition][]. There’s a bit more it to the brief bio I put on
the website.

I guess it all started when I was young, around the age of four, my
parents were given a Commodore 64 by my Uncle - it came with a Tape deck
and a cartridge game called [Tooth Invaders][]. Interesting game, you
were given a set of teeth to look after and you had to fight germs off
the teeth with a toothbrush. Occasionally flossing between the teeth and
you had to avoid a little green dude who would kick your ass if he
touched you. A couple of years passed and there were a couple of people
at school who also had a Commodore 64, we would borrow games and play
them. My Dad came back from Canada after visiting his father with a box
full with games, one that I got really addicted to was [Maniac
Mansion][]. Would have to say one of my most favorite games in the
entire fucking world. You could choose 2 out of 6 players (you get stuck
with Dave, the most useless guy ever) each of the alternative 6 players
had different skills and abilities, which also meant you could complete
the game in many different ways. This formed my love for click adventure
games. Along with Maniac Mansion, was [Labyrinth][]- I guess a pre-scumm
game, but it was very interactive and fun, also based on the movie. I
guess this is where my love for gaming came from. Also playing Granny’s
Garden on the BBC’s in primary school. That game scared the hell outta
me, maybe it was her big pixel nose that bothered me. I don’t really
remember.

Later in life, I got access to a PC, 486 I think from memory. After
school I used to create games in ZZT. As well as rollerskating. That was
all the rage back then. Pretty sure I made a ZZT game about Rolloways.
But anyway, I became more interested in other aspects of computing, such
as hacking. I remember sitting in the computer labs at school, we only
had 486s - oh man we played STUNTS on them while the teachers were out,
it was the fucking bomb! Pretty sure I managed to install a virus on one
of the machines, [Ping-pong][] and the teacher freaked out. Our school
wasn’t exactly computer literate.

I remember when we first got the internet at home. It was a dial-up
shell account on Multiline. First browser I ever used? Lynx. When I was
14, I remember printing out pages and pages of things to read, actually
from the Triple J website (lets see if we can use the wayback machine on
it.. [oh yeah..][] not that far though. All my friends were confused and
didn’t quite understand the use for it, I loved it!

Later, in year 11 I discovered something called IRC.. ended up getting a
bit too addicted to that and spent most my time online chatting to
people, writing scripts in mIRC and making new friends on AU's Undernet
server.. which ended up splitting off to oz.org because of so many
netsplits. I lurked in channel #Perth and #Perth-16-20 (lol) most. Too
much time online ended my studies at high school pretty early in year
11. I started hanging around with an older crowd, students at UWA and
some other people. This is where I got introduced into lasertag, by a
guy called RoaDKiLL. Every Monday night we would rock up to Zone 3
Northbridge for a weekly competition. Back then they were using the old
V4 model P&C packs, which, are still the greatest zone packs on earth.
Why? THE TIMING IS SO DAMN PRECISE (the newer infusion packs, the ones
they have now at Darkzone, are not as responsive - because of the radio
that enables the live scoring, it needs to talk back and forth to the
server).

I became slack during this time, not doing much with my life. I was
being very social online and not so much outside, except for the
occasional rollerskating on Friday nights. Yes - I love skating, as you
might have already guessed. The rush of skating fast and dodging people
is amazing. Back then I remember playing mutiplayer Heretic with one of
my UWA mates, over a 9600bps modem, it was fantastic. Also back then I
was getting into the Lucasarts click adventure games, Monkey Island - my
most favorite game in the universe. As I wasn't doing much, my step mum
got on my case, ended up getting me an apprenticeship at the company
which she had recently purchased, Comfix. Back then there were no real
IT courses so the apprenticeship I had to do was called Electronic
Servicing. At Comfix I was repairing Commodore 64s, Commodore Amigas,
Lynx Gamegears, PCs and other really old machines. I learnt so much from
that place, which inspired my love for gadgets :~)

Later on, I found myself out of work then decided to look more into
programming. Found myself working on a business application written in
Win32-C. Yet was later replaced with a perl script. I begin hanging out
with a couple of my mates, one of them was doing studying computer
science at Murdoch -- another just chilling out. We used to play
Starcraft every night, for weeks on end. I felt like I was doing
nothing, I thought I might head back to what I know, IT support and to
attend university at the same time.

Later, one of my mates asked me if I was interested in working in a web
development role. I enthusiastically said OH HELL YEAH and went to work
for them. That's when I was first introduced into PHP and ASP
programming. I became more and more interested in what I could do with
web related services and databases. This was maybe one of my major
turning points down to what I am currently doing today, working as a
Programmer at a local ISP.

Gaming wise, I’ve just started to get into Lord of the Rings Online.
It’s free so I thought I might give it a chance, so far it’s okay – I’m
playing it with my fiancée, who is the most awesome and beautiful person
in the entire whole world – but I’ll post more about her later ;\~).
Infact we recently just moved out together. I used play World of
Warcraft quite a bit. I had a Warlock, Paladin, Druid and some other
characters all up to 80. Once I hit 80 I wasn’t quite sure what to do,
because I simply can’t stand raiding. It’s boring, repetitive and a
major timesink. I did, however, enjoy world PVP, griefing is fun! – So I
learnt from playing hours and hours of EVE Online with a certain
corporation called GoonFleet. Industrial espionage in a MMORPG? oh hell
yeah. We had a spymaster, [here is an article][] on his work in the
game. I was one of the guys that stepped up and snuck an alt player into
one of our rival corporations, Lokta Veritas. It was bit of a fun
experience, trying to act undercover. I setup a PHP script on my server
at home that posted everything that happened into LV’s corporation/gang
chat window, so the spymaster could process the information and relay it
on to the people who needed the information. GoonFleet was a lot of fun,
just being a general jerk in EVE Online surrounded by others who took it
so so seriously.

I enjoy making my life easier around the house, and smashing up scripts
in PERL, BASH and PHP. I wouldn't go to the extent of creating an IP
controlled toaster, but I would go to the effort of setting up a TV box
with XBMC - with notifications being pushed to it. Or gathering usage
from various items around the house. I love data. DATA DATA DATA DATA,
but not the Brent Spiner kinda data. He was always an awkward android.

I'm pretty big into scifi's and that kinda pop-culture thing. I watched
the entire Next Generation series while sitting at my desk at my old
job, while smashing out code for the website we were working on. My
favorite store would have to be Minotaur in Melbourne. That place is god
damn amazing.

I have a secret love for typography. mmmMmm typography.
hell-vetica-yeah.

Not to mention, I'm also a bit of a music geek. If I've heard the band
before I can usually guess who they are, not to mention being able to
spot known tunes within at least 15 second of the song actually playing.
I love music.

Puns? I love puns. Puns are amazing.

I'm looking at doing some development work on the HTC Desire HD -
android based phone. I have a few ideas and I'm going to see what I can
come up with. Of course I'll post what I make on my blog. (This thing,
right here, you're on it now)

So far I've posted on Twitter and Facebook, asking my mates to share my
iiNet's TopGeek vote url. As of yesterday I launched a banner campaign
on SomethingAwful as well as linking my URL on Delicious and Reddit.

Vote for me please, I really want to beat Simon Hackett.

  [iiNet’s TopGeek competition]: http://bit.ly/spyng33k
  [Tooth Invaders]: http://www.lemon64.com/games/details.php?ID=2677&coverID=1634
  [Maniac Mansion]: http://en.wikipedia.org/wiki/Maniac_Mansion
  [Labyrinth]: http://www.thehouseofgames.net/index.php?t=10&id=408
  [Ping-pong]: http://en.wikipedia.org/wiki/Ping-Pong_virus
  [oh yeah..]: http://web.archive.org/web/*/http://www.triplej.net.au
  [here is an article]: http://www.tentonhammer.com/node/68172
