Title: Jocelyn's Horroscope 
Date: 2007-05-11 13:29
Author: mattie
Category: Joce
Slug: joce-horroscope
Summary: Jocelyn's horroscope

Introduction
This report is a short edition of the AstroText Youth. It is meant as a sample and advertisement for the full version of the AstroText Youth which can be ordered from Astrodienst as a downloadable E-Horoscope of about 20 - 30 pages.

In the short edition, only a few, but nevertheless important aspects of your natal chart are considered.
The report was generated with the following birth data: female, born on March 2007 in Perth, Australia.

Your sun sign is Pisces. This is the sign in which the Sun is in your birth chart. Your Ascendant, the rising sign, is in Leo, and your Moon is in Sagittarius.

Leo Rising
You like to be the center of attention, to appear strong, confident and dominant, even if something is bothering you.
You like to be the leader, and you can be a good one as long as you remember that leadership is a responsibility as well as power and privilege.
You can be stubborn, especially if your pride is involved. You are idealistic, and people have to prove to you that they have integrity.
You like to have rich and elegant things that you can show off to others. But remember that just because something impresses people, it is not necessarily good. You need to learn the difference.
Others may or may not see you as beautiful or handsome, but as you grow up you will develop a regal bearing that will make others feel that you are dependable. Many people will like you, and most will also respect you.

Sun in Pisces
You are a very sensitive and emotional person who quickly picks up moods and emotions from other people and makes them part of your own. This trait makes it very easy for others to hurt your feelings. Because you have the ability to put yourself in someone else's place, you have an exceptional understanding of other people's needs. Whenever possible, you try to help others, because it makes you feel good about yourself.
Very often you like to go into your own private fantasy world and think about ideas that mean something only to you. Just don't spend so much time there that you lose track of what is happening outside in the real world.
You may be somewhat shy, because you feel you have to trust people before you can really open up to them. But even though you are shy, you do need other people, for without them you feel lonely, even in your own private world.

Sun in the Seventh House
You are at your best with another person, either working or just having a good time. You are able to adjust your own needs to someone else's so that together you make a pair that works better than either of you would separately.
You are very social, and you have learned early in life to get along with others. For this reason, you are able to help others patch up an argument, because you see both sides of the controversy.
Some people express another side of the seventh-house Sun. You may really feel like arguing and fighting with others, instead of trying to get along. Don't make a habit of it, because everyone gets tired of fighting eventually.
You should learn to be more independent. Also avoid making compromises that require you to give up something essential just to keep a friend.

Moon in Sagittarius
You are an idealist who wants the world to be grand and good and beautiful. You want people to be good and noble and are very disappointed when they are not.
You are so concerned with the important things in life that sometimes you forget about little matters that really have to be done. You are very independent and resent anyone who tries to keep you from doing what you want.
You are optimistic and cheerful, and you can't stay sad for very long. Usually you feel good about life, and you try to make others around you feel that way also. People will like you for this.
When you are an adult, you will be interested in any subject that teaches you more about the universe and your place in it.

Moon in the Fourth House
To be at your best, you need to have a peaceful home life, for this means emotional security to you. When you feel strong and secure, you are very kind and sensitive to other people's feelings. But if you feel depressed or insecure, you tend to withdraw into your own private world.
Your mother is very important to you, and you need to be close to her.
You are interested in the past, and you may become an eager student of history, especially the history of your own family origins and background.

Venus in Aries
You are very affectionate toward others, but you hate to be tied down by anyone. Not shy or reserved, you express your loving feelings freely, and you need to be free to love whomever you choose. You are unwilling to give in to your partner, because you always want things your own way. This could be a problem in your friendships. However, now is the time when you can learn how to give and how to make compromises with friends, relatives and other people.
People usually like you because you are yourself, not because you are charming. You know what you want, and you are willing to go after it.

Venus in the Eighth House
You enjoy your senses, and you want to experience the world as vividly and totally as possible. You seem to be looking for a hidden dimension of the world that will transform your life completely. Often you will seek this dimension through relationships with others, which will be very intense and full of feeling.
Be careful not to be too possessive of your friends. Also, you shouldn't choose your friends according to the advantages you will gain from their friendship.
While you are young, you should begin to take care of your health by not eating or drinking too much and by getting plenty of physical exercise.

Mars in Aquarius
Rather than working according to established patterns, you want to find your own way, which you hope will be better. As a result you are quite inventive and original
As you get older you will be concerned with the greater good of society. You are quite capable of directing your efforts to goals that will not benefit you personally but will benefit others. Even while you are young, you resent authority.
If you are convinced of the importance of a cause, you work better with a group than you do alone.

Mars in the Sixth House
You enjoy hard work, as long as you can control the way you do it. You are quite disciplined in your own work habits, but you don't like to work with people who are less disciplined. But if you respect those who have authority over you, if they have demonstrated that they do know more than you and can teach you something, you will be an excellent employee. However, when you are older, your work drive will be expressed most smoothly if you are self-employed.
When working with others, especially if you are the leader, try not to be too critical. With this placement, it is very important that you learn to express your honest feelings of anger.
