<?php
/** spyn: this was originally from http://behindcompanies.com/2014/01/a-simple-script-for-deploying-code-with-githubs-webhooks/
  *       but I've modified it so I can use https instead of having to worry about ssh keys and shit.
  *       I've also modified it to automatically build my blog from pelican.
  *
  * This script is for easily deploying updates to Github repos to your local server. It will automatically git clone or 
  * git pull in your repo directory every time an update is pushed to your $BRANCH (configured below).
  * 
  * INSTRUCTIONS:
  * 1. Edit the variables below
  * 2. Upload this script to your server somewhere it can be publicly accessed
  * 3. Make sure the apache user owns this script (e.g., sudo chown www-data:www-data webhook.php)
  * 4. (optional) If the repo already exists on the server, make sure the same apache user from step 3 also owns that 
  *    directory (i.e., sudo chown -R www-data:www-data)
  * 5. Go into your Github Repo > Settings > Service Hooks > WebHook URLs and add the public URL 
  *    (e.g., http://example.com/webhook.php)
  *
**/

// Set Variables
$LOCAL_ROOT         = "/var/www/apps";
$LOCAL_REPO_NAME    = "mattie-blog";
$LOCAL_REPO         = "{$LOCAL_ROOT}/{$LOCAL_REPO_NAME}";
$REMOTE_REPO        = "https://spyn@bitbucket.org/spyn/mattie-blog.git";
$BRANCH             = "master";

$JSON = $_POST['payload'];

if ( $JSON ) {
  // Only respond to POST requests from ButtBucket
  $dj = json_decode($JSON);

  if ( $dj->{'canon_url'} == "https://bitbucket.org" ) {

    if( file_exists($LOCAL_REPO) ) {
      // If there is already a repo, just run a git pull to grab the latest changes
      shell_exec("cd {$LOCAL_REPO} && git pull");
      shell_exec("cd {$LOCAL_REPO} && make clean && make html");

      die("done " . mktime());
    } else {
      // If the repo does not exist, then clone it into the parent directory
      shell_exec("cd {$LOCAL_ROOT} && git clone {$REMOTE_REPO}");
      shell_exec("cd {$LOCAL_REPO} && make clean && make html");

      die("done " . mktime());
    }
  }
}

?>